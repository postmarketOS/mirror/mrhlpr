#!/bin/sh -e
# Description: Build native libraries of mrhlpr and mrtest using mypyc
# https://postmarketos.org/pmb-ci

if [ "$(id -u)" = 0 ]; then
	set -x
	apk -q add gcc musl-dev py3-argcomplete py3-gitlab py3-pip python3-dev
	exec su "${TESTUSER:-build}" -c "sh -e $0"
fi

set -x

# mypyc from Alpine may be an old version. Given mypyc's early development
# status, install it from pypi instead to ensure we don't have to deal with
# working around bugs that already have been fixed in newer versions.
pip install --break-system-packages --no-warn-script-location mypy
python -m mypyc mrtest mrhlpr