# Copyright 2022 Oliver Smith
# SPDX-License-Identifier: GPL-3.0-or-later
"""High level merge request related functions on top of git, gitlab, mrdb, pipeline."""

import json
import logging
import os
import re
import shlex
import subprocess
import sys
import time
from dataclasses import dataclass
from typing import Any, Final, Optional

try:
    import gitlab as gitlab_api
except ImportError:
    pass

from mrhlpr.origin import GitLabOrigin
from mrhlpr.pipeline import PipelineMetadata
from . import ci_labels  # type: ignore[attr-defined]
from . import git
from . import gitlab
from . import mrdb


def checked_out() -> int:
    """:returns: checked out MR ID or None"""
    origin = gitlab.parse_git_origin()
    branch = git.branch_current()
    mr_id = mrdb.get(origin.host, origin.project_id, branch)
    if not mr_id:
        print("ERROR: no merge request is currently checked out.")
        print("Run 'mrhlpr checkout N' first (N is the MR-ID).")
        exit(1)
    return mr_id


@dataclass
class MergeRequestStatus:
    title: str
    source_branch: str
    target_branch: str
    source: str
    source_namespace: str
    source_project_id: int
    allow_push: bool
    state: str


def get_status(
    mr_id: int, no_cache: bool = False, origin: Optional[GitLabOrigin] = None
) -> MergeRequestStatus:
    """Get merge request related information from the GitLab API.
    To hack on this, run mrhlpr with -v to get the cached JSON files
    location. Then you can take a look at the data returned from the API.

    :param mr_id: merge request ID
    :param no_cache: do not cache the API result for the merge request data
    :param origin: gitlab origin information, see gitlab.parse_git_origin()
    :returns: a MergeRequestStatus object like:
              title: "This is my first merge request"
              source_branch: "mymr"
              target_branch: "v20.05"
              source: "ollieparanoid/mrhlpr"
              source_namespace: "ollieparanoid"
              source_project_id: 12345
              allow_push: True
              state: "merged"
    """
    # Query merge request
    # https://docs.gitlab.com/ee/api/merge_requests.html
    if not origin:
        origin = gitlab.parse_git_origin()
    url_mr = "/projects/{}/merge_requests/{}".format(origin.api_project_id, mr_id)
    api = origin.download_json(url_mr, no_cache)

    # Query source project/repository
    # https://docs.gitlab.com/ee/api/projects.html
    # Always cache this, since we don't expect the "path_with_namespace" to
    # ever change (and even if, we can keep using the old one locally).
    url_project = "/projects/" + str(api["source_project_id"])
    api_source = origin.download_json(url_project)

    # Allow maintainer to push
    allow_push = False
    if "allow_maintainer_to_push" in api and api["allow_maintainer_to_push"]:
        allow_push = True

    # MR initiated from same GitLab project
    if api_source["namespace"]["name"] == origin.project:
        allow_push = True

    # Sanity checks (don't let the API trick us into passing options to git!)
    source = api_source["path_with_namespace"]
    if not re.compile(r"[a-zA-Z0-9_.-]*\/[a-zA-Z0-9_.-]*").match(source) or source.startswith("-"):
        print("Invalid source: " + source)
        exit(1)

    source_namespace = api_source["namespace"]["name"]
    if not re.compile(r"[a-zA-Z0-9_.-]*").match(source_namespace) or source_namespace.startswith(
        "-"
    ):
        print("Invalid source_namespace: " + source_namespace)
        exit(1)

    source_branch = api["source_branch"]
    target_branch = api["target_branch"]
    for branch in [source_branch, target_branch]:
        if not re.compile(r"[a-zA-Z0-9/-_.]*").match(branch) or branch.startswith("-"):
            print(f"Invalid branch: {branch}")
            exit(1)

    return MergeRequestStatus(
        api["title"],
        source_branch,
        target_branch,
        source,
        source_namespace,
        api["source_project_id"],
        allow_push,
        api["state"],
    )


def get_url(mr_id: int, origin: Optional[GitLabOrigin] = None) -> str:
    """Get the URL to the merge request on the GitLab server.

    :param mr_id: merge request ID
    :param origin: gitlab origin information, see gitlab.parse_git_origin()
    :returns: URL to the merge request"""
    if not origin:
        origin = gitlab.parse_git_origin()
    return f"https://{origin.host}/{origin.project_id}/-/merge_requests/{mr_id}"


def get_artifacts_zip(mr_id: int, no_cache: bool, origin: GitLabOrigin) -> str:
    """Download artifacts from the GitLab API.

    :param mr_id: merge request ID
    :param no_cache: do not cache the API result for the merge request data
    :param origin: gitlab origin information, see gitlab.parse_git_origin()
    :returns: path to downloaded artifacts.zip file
    """
    pipeline_metadata = PipelineMetadata(mr_id, no_cache, origin)
    # Download artifacts zip (with cache)
    return gitlab.download_artifacts_zip(
        origin.api,
        pipeline_metadata,
    )


class UnknownReleaseError(ValueError): ...


def _target_branch_to_pmos_release(target_branch: str) -> str:
    if target_branch == "master":
        return "edge"

    pattern = re.compile(r"^v\d\d\.\d\d$")

    if pattern.match(target_branch):
        return target_branch

    raise UnknownReleaseError


def get_artifacts_repo_urls(
    mr_id: int, no_cache: bool, origin: GitLabOrigin, alpine_mr: bool
) -> list[str]:
    """Get a list of URLs that can be used as repositories for apk. These URLs contain
    the apks built as part of the latest job for the host system's architecture in the
    given MR."""
    pipeline_metadata = PipelineMetadata(mr_id, no_cache, origin)
    url_mr = "/projects/{}/merge_requests/{}".format(
        origin.api_project_id,
        mr_id,
    )
    api = origin.download_json(url_mr, no_cache=True)

    host_arch_job_web_url = pipeline_metadata.host_arch_job["web_url"]
    if alpine_mr:
        return [
            f"{host_arch_job_web_url}/artifacts/raw/packages/main",
            f"{host_arch_job_web_url}/artifacts/raw/packages/community",
            f"{host_arch_job_web_url}/artifacts/raw/packages/testing",
        ]
    else:
        pmos_release = _target_branch_to_pmos_release(api["target_branch"])

        # FIXME: This is just hard-coding extra repos. While we aren't likely to add
        # more ones in the future, it'd be better to not do this.
        return [
            f"{host_arch_job_web_url}/artifacts/raw/packages/{pmos_release}",
            f"{host_arch_job_web_url}/artifacts/raw/packages/systemd-{pmos_release}",
        ]


def checkout(
    mr_id: int, no_cache: bool = False, fetch: bool = False, overwrite_remote: bool = False
) -> None:
    """Add the MR's source repository as git remote, fetch it and checkout the
    branch used in the merge request.

    :param mr_id: merge request ID
    :param no_cache: do not cache the API result for the merge request data
    :param fetch: always fetch the source repository
    :param overwrite_remote: overwrite URLs of existing remote"""
    status = get_status(mr_id, no_cache)
    remote, repo = status.source.split("/", 1)
    origin = gitlab.parse_git_origin()
    branch = status.source_branch

    # Require clean worktree
    if not git.clean_worktree():
        print("ERROR: worktree is not clean! Commit or stash your changes")
        print("and try again. See 'git status' for details.")
        exit(1)

    # Don't add the origin remote twice
    remote_local = remote
    if remote == origin.project:
        remote_local = "origin"

    # Check existing remote
    project_repo_git = "{}/{}.git".format(remote, repo)
    username = origin.username + "@" if origin.username else ""
    url = "https://" + username + origin.host + "/" + project_repo_git
    url_push = "git@" + username + origin.host + ":" + project_repo_git
    existing = git.get_remote_url(remote_local)
    if existing and existing != url:
        if overwrite_remote:
            print("Overwriting remote URL (old: '" + existing + "')")
            git.run(["remote", "set-url", remote_local, url])
            git.run(["remote", "set-url", "--push", remote_local, url_push])
        else:
            print("ERROR: Remote '" + remote_local + "' already exists and has a different URL.")
            print()
            print("existing: " + existing)
            print("expected: " + url)
            print()
            print(
                "If you are fine with the expected url, use 'mrhlpr checkout"
                " " + str(mr_id) + " -o' to overwrite it."
            )
            print()
            print("mrhlpr will also set this pushurl: " + url_push)
            exit(1)

    # Fetch origin
    if fetch and remote != origin.project:
        origin_url = git.get_remote_url()
        if not origin_url:
            msg = "Couldn't get URL for origin"
            raise RuntimeError(msg)
        print("Fetch " + origin_url)
        git.run(["fetch", "origin"])

    # Add missing remote
    if not existing:
        git.run(["remote", "add", remote_local, url])
        git.run(["remote", "set-url", "--push", remote_local, url_push])
        fetch = True
    if fetch:
        print("Fetch " + url)
        try:
            git.run(["fetch", remote_local])
        except subprocess.CalledProcessError:
            print(
                "Failed to fetch from remote. Try running 'git fetch "
                + remote_local
                + "' manually and check the output, most"
                " likely you ran into this problem:"
                " https://gitlab.postmarketos.org/postmarketOS/mrhlpr/issues/1"
            )
            sys.exit(1)

    branch_local = "mrhlpr/" + str(mr_id)

    # Checkout the branch
    print("Checkout " + branch_local + " from " + remote + "/" + branch)
    if branch_local in git.branches():
        # Check existing branch
        remote_existing = git.branch_remote(branch_local)
        if remote_existing != remote_local:
            print("Branch '" + branch_local + "' exists, but points to a different remote.")
            print()
            print("existing remote: " + str(remote_existing))
            print("expected remote: " + remote_local)
            print()
            print("Consider deleting this branch and trying again:")
            print("$ git checkout master")
            print("$ git branch -D " + branch_local)
            print("$ mrhlpr checkout " + str(mr_id) + " -n")
            exit(1)
        git.run(["checkout", branch_local])

        # Compare revisions (reset hard if needed)
        rev_current = git.run(["rev-parse", "HEAD"])
        rev_remote = git.run(["rev-parse", remote_local + "/" + branch])
        if rev_current == rev_remote:
            print("(Most recent commit is already checked out.)")
        elif rev_current is not None and rev_remote is not None:
            print("################")
            print("NOTE: branch " + branch_local + " already exists, reusing.")
            print("You can go back to the previous commit with:")
            print("$ git reset --hard " + rev_current)
            print("################")
            git.run(["reset", "--hard", rev_remote])
        else:
            msg = "rev_current and rev_remote were not both not None"
            raise RuntimeError(msg)
    else:
        git.run(["checkout", "-b", branch_local, remote_local + "/" + branch], check=False)
        if git.branch_current() != branch_local:
            print()
            print("ERROR: checkout failed.")
            print("* Does that branch still exist?")
            print("* Maybe the MR has been closed/merged already?")
            print("* Do you have unstaged commits that would be overwritten?")
            exit(1)

    # Set upstream branch (git will still complain with "The upstream branch
    # of your current branch does not match the name of your current branch",
    # unless "git config push.default upstream" is set. There doesn't seem to
    # be a way around that.)
    git.run(["branch", "-u", remote_local + "/" + branch])

    # Save in mrdb
    mrdb.set(origin.host, origin.project_id, branch_local, mr_id)


def commits_have_mr_id(commits: list[str], mr_id: int) -> bool:
    """Check if all given commits have the MR-ID in the subject.

    :param commits: return value from git.commits_on_top_of()
    :returns: True if the MR-ID is in each subject, False otherwise"""
    mr_url = get_url(mr_id)
    for commit in commits:
        body = git.run(["show", "-s", "--format=%b", commit])
        if body is None:
            return False
        if f"Part-of: {mr_url}" not in body:
            return False
    return True


def commits_follow_format(commits: list[str]) -> tuple[Optional[bool], list[Any]]:
    """Check if the commit subjects follow the correct naming format.

    :param commits: return value from git.commits_on_top_of()
    :returns:
        - result: True if the commits are formatted correctly, False if
          something is obviously wrong and None if it is something between
        - subject_err: string with commit hash and explanation of what
          is wrong with the subject
    """
    subjects = {}
    for commit in commits:
        ret = git.run(["show", "-s", "--format=%s", commit])
        if ret is None:
            msg = "Something went wrong when getting the commit message"
            raise RuntimeError(msg)
        subjects[commit] = ret

    # Run generic checks that don't need definitions first
    for commit, subject in subjects.items():
        # Don't have an period at the end of the subject
        if subject.endswith("."):
            return (False, [commit[0:6] + " ends with period"])

    # Load a definition file from the root of the repo if it exists
    definition_file = os.path.join(git.topdir(), ".mrhlpr.json")
    if not os.path.isfile(definition_file):
        return (True, [])

    with open(definition_file) as handle:
        definitions = json.load(handle)

    regexes_pass = []
    for regex in definitions["subject_format"]["pass"]:
        regexes_pass.append(re.compile(regex))

    regexes_unknown = []
    for regex in definitions["subject_format"].get("unknown", []):
        regexes_unknown.append(re.compile(regex))

    result: Optional[bool] = True
    subj_err = []

    for commit, subject in subjects.items():
        logging.debug("Checking subject: {}".format(subject))
        for regex in regexes_pass:
            if regex.match(subject):
                logging.debug("  Matched pass regex {}".format(regex.pattern))
                break
        else:
            for regex in regexes_unknown:
                if regex.match(subject):
                    logging.debug("  Matched unknown regex {}".format(regex.pattern))
                    result = None
                    subj_err.append(commit[0:6] + " matches " + regex.pattern)
                    break
            else:
                logging.debug("  No regex matched")
                return (False, [commit[0:6] + " doesn't match any regex"])

    return (result, subj_err)


def commits_are_signed(commits: list[str]) -> bool:
    """Check if all given commits are signed.

    :param commits: return value from git.commits_on_top_of()
    :returns: True if all are signed, False otherwise"""
    for commit in commits:
        if not git.run(["verify-commit", commit], check=False):
            return False
    return True


def run_git_filter_branch(
    script: str, from_ref: str, mr_id: Optional[int] = None, label: Optional[str] = None
) -> None:
    script = os.path.realpath(os.path.realpath(__file__) + f"/../data/{script}")
    env = os.environ.copy()
    env["FILTER_BRANCH_SQUELCH_WARNING"] = "1"
    if mr_id:
        env["MRHLPR_MSG_FILTER_MR_ID"] = get_url(mr_id)
    if label:
        env["MRHLPR_LABEL"] = ci_labels[label]

    try:
        git.run(
            [
                "filter-branch",
                "-f",
                "--msg-filter",
                f"python3 {shlex.quote(script)}",
                "--commit-filter",
                'git commit-tree -S "$@"',
                f"{from_ref}..HEAD",
            ],
            env=env,
        )
    except subprocess.CalledProcessError:
        print(
            "ERROR: git filter-branch failed. Do you have git commit signing"
            " set up properly? (Run with -v to see the failing command.)"
        )
        exit(1)


def fixmsg(
    mr_id: int, skip_build: bool = False, ignore_count: bool = False, skip_vercheck: bool = False
) -> None:
    """Add the MR-ID in each commit of the MR.

    :param mr_id: merge request ID"""
    target_branch = get_status(mr_id).target_branch

    ret = git.run(["rev-parse", "--show-toplevel"])
    if ret is None:
        msg = "Something went wrong when getting the repository directory"
        raise RuntimeError(msg)
    os.chdir(ret)

    print("Adding ' Part-of: !" + str(mr_id) + "' trailer to commits and signing them...")  # noqa: E501
    run_git_filter_branch("msg_filter_add_commit_id.py", f"origin/{target_branch}", mr_id)

    if skip_build:
        print("Appending [ci:skip-build] to last commit...")
        run_git_filter_branch("msg_filter_add_label.py", "HEAD~1", label="skip_build")
    if ignore_count:
        print("Appending [ci:ignore-count] to last commit...")
        run_git_filter_branch("msg_filter_add_label.py", "HEAD~1", label="ignore_count")
    if skip_vercheck:
        print("Appending [ci:skip-vercheck] to last commit...")
        run_git_filter_branch("msg_filter_add_label.py", "HEAD~1", label="skip_vercheck")


def push(mr_id: int, no_cache: bool = False) -> None:
    print("Pushing changes...")
    status = get_status(mr_id, no_cache)
    origin = gitlab.parse_git_origin()
    remote_local = status.source.split("/", 1)[0]
    if remote_local == origin.project:
        remote_local = "origin"
    git.push_head(remote_local, status.source_branch)


def finish(
    mr_id: int, comment: Optional[str], no_cache: bool = False, skip_approve: bool = False
) -> None:
    if "gitlab" not in sys.modules:
        logging.error("python-gitlab is needed to run 'finish'")
        exit(1)

    gitlab_token = os.environ.get("GITLAB_TOKEN")

    if not gitlab_token:
        logging.error(
            "To use 'finish' you need a personal access token set in the environment as GITLAB_TOKEN. See https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html."
        )
        exit(1)

    push(mr_id, no_cache)
    gl = gitlab_api.Gitlab(url="https://gitlab.postmarketos.org", private_token=gitlab_token)
    try:
        gl.auth()
    except gitlab_api.exceptions.GitlabAuthenticationError as authentication_exception:
        logging.error(
            "Authenticating with the token from GITLAB_TOKEN in the environment produced the following error: %s\n\nSee https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html",
            authentication_exception,
        )
        exit(1)

    origin = gitlab.parse_git_origin()
    project = gl.projects.get(origin.project_id)
    mr = project.mergerequests.get(mr_id)
    if skip_approve or (gl.user and mr.author and gl.user.id == mr.author.get("id")):
        print("Skipping approval...")
    else:
        if mr.approvals.get().attributes["user_has_approved"]:
            print("MR has already been approved by you!")
        else:
            print("Approving MR...")
            try:
                mr.approve(git.sha())
            except Exception as e:
                raise RuntimeError("Approval failed, your approval was not added!") from e

    if comment:
        print("Adding the comment...")
        mr.notes.create({"body": comment})

    iterations: Final[int] = 10
    sleep_time: Final[int] = 5
    # Proper stupid but it works. Originally I had this set to a maximum of 15 seconds
    # of sleepy time, but for gitlab.postmarketos.org this wasn't always sufficient to catch up.
    print(f"Setting to auto-merge (may take up to {iterations * sleep_time} seconds)...")
    for _ in range(iterations):
        try:
            # Give gitlab some time to process the push, it sometimes struggles
            time.sleep(sleep_time)
            mr.merge(merge_when_pipeline_succeeds=True)
            return
        except gitlab_api.exceptions.GitlabMRClosedError:
            pass
    print("Failed set auto merge! Is the MR open and the GitLab server responsive?")
