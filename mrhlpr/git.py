# Copyright 2022 Oliver Smith
# SPDX-License-Identifier: GPL-3.0-or-later
"""Low level git functions."""

import subprocess
import logging
from typing import Optional


def run(
    parameters: list[str], env: Optional[dict[str, str]] = None, check: bool = True
) -> Optional[str]:
    """Run a git command.

    :param parameters: list of arguments to pass to git
    :param env: environment variables passed to the process
    :param check: when set to True, raise an exception on exit code not
                  being 0
    :returns: on success: output of the command (last new line removed)
              on failure: None"""
    try:
        logging.debug("+ git " + " ".join(parameters))
        stdout = subprocess.check_output(["git"] + parameters, env=env, stderr=subprocess.STDOUT)
        ret = stdout.decode("utf-8").rstrip()
        logging.debug(ret)
        return ret
    except subprocess.CalledProcessError as e:
        ret = e.output.decode("utf-8").rstrip()
        logging.debug(ret)
        if check:
            raise
        return None


def get_remote_url(remote: str = "origin") -> Optional[str]:
    """:returns: the remote URL as string, e.g. "https://gitlab.postmarketos.org/postmarketOS/pmaports.git" """
    ret = run(["remote", "get-url", remote], check=False)
    return ret


def branches(obj: str = "refs/heads") -> list[str]:
    """:returns: a list of all local branch names"""
    ret = run(["for-each-ref", obj, "--format", "%(refname:short)"])
    if ret is None:
        msg = "Something went wrong when getting all local branch names"
        raise RuntimeError(msg)
    return ret.splitlines()


def branch_current() -> str:
    """:returns: current branch name (if any) or "HEAD" """
    ret = run(["rev-parse", "--abbrev-ref", "HEAD"])
    if ret is None:
        msg = "Something went wrong when getting current branch name"
        raise RuntimeError(msg)
    return ret


def branch_remote(branch_name: str = "HEAD") -> Optional[str]:
    """:returns: remote name, or None"""
    upstream = run(["rev-parse", "--abbrev-ref", branch_name + "@{u}"], check=False)
    if upstream:
        return upstream.split("/", 1)[0]
    return None


def commits_on_top_of(branch_name: str = "master") -> list[str]:
    """:returns: list of commit ID strings"""
    ret = run(["rev-list", f"origin/{branch_name}..HEAD"])
    if ret is None:
        msg = "Something went wrong when getting list of commit ID strings"
        raise RuntimeError(msg)
    return ret.splitlines()


def is_rebased(branch_name: str = "master") -> bool:
    """Check if the current branch needs to be rebased on a given branch."""
    return run(["rev-list", "--count", f"HEAD..origin/{branch_name}"]) == "0"


def clean_worktree(print_warning: bool = False) -> bool:
    """Check if there are not modified files in the git dir."""
    worktree_untracked_files = []
    worktree_status = run(["status", "--porcelain=v2"])
    worktree_clean = True

    if worktree_status is None:
        msg = "Something went wrong when running git status"
        raise RuntimeError(msg)

    for line in worktree_status.splitlines():
        # ? means it's an untracked file, which we don't need to worry about
        # when switching branches. However, any other file status is a
        # problem, so report the worktree as unclean if one appears.
        if line[0] == "?":
            worktree_untracked_files.append(line[2:])
        else:
            worktree_clean = False

    if worktree_untracked_files and worktree_clean and print_warning:
        print()
        print("Warning: There are untracked files")
        for file in worktree_untracked_files:
            print(f"* {file}")
        print()

    return worktree_clean


def topdir() -> str:
    """:returns: current branch name (if any) or "HEAD" """
    ret = run(["rev-parse", "--show-toplevel"])
    if ret is None:
        msg = "Something went wrong when getting current branch name"
        raise RuntimeError(msg)
    return ret


def sha() -> str:
    """:returns: current SHA"""
    ret = run(["rev-parse"])
    if ret is None:
        msg = "Something went wrong when getting current commit SHA"
        raise RuntimeError(msg)
    return ret


def push_head(remote: str, branch: str) -> None:
    """Push HEAD to branch in remote"""
    ret = run(["push", "--force-with-lease", remote, "HEAD:" + branch])
    if ret is None:
        msg = f"Something went wrong when pushing HEAD to branch '{branch}' in remote '{remote}'"
        raise RuntimeError(msg)
    print(ret)
