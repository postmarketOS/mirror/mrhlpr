# Copyright 2022 Oliver Smith
# SPDX-License-Identifier: GPL-3.0-or-later
"""GitLab related functions on top of git."""

import hashlib
import urllib.parse
import urllib.request
import os
import shutil
import logging
import re

from mrhlpr.origin import GitLabOrigin
from . import git
from . import runner
from .pipeline import PipelineMetadata


def download_artifacts_zip(api: str, pipeline: PipelineMetadata, no_cache: bool = False) -> str:
    """Download the job artifacts zip file, with a cache.

    :param api: gitlab API url, from parse_git_origin()["api"]
    :param pipeline: information about the pipeline
    :param no_cache: download again, even if already cached
    :returns: path to downloaded zip file"""

    runner.verify(pipeline.host_arch_job["runner"])

    url = f"{api}/projects/{pipeline.project_id}/jobs/{pipeline.host_arch_job['id']}/artifacts"

    # Prepare cache
    cache_dir = f"{os.getenv('HOME')}/.cache/mrhlpr/artifacts"
    cache_key = hashlib.sha256(url.encode("utf-8")).hexdigest()
    cache_file = f"{cache_dir}/{cache_key}"
    os.makedirs(cache_dir, exist_ok=True)

    # Check the cache
    if os.path.exists(cache_file) and not no_cache:
        logging.debug(f"Download {url} (cached)")
    else:
        print(f"Download {url}")
        # Save to temp file
        temp_file = cache_file + ".tmp"
        with urllib.request.urlopen(url) as response:
            with open(temp_file, "wb") as handle:
                shutil.copyfileobj(response, handle)

        # Replace cache file
        if os.path.exists(cache_file):
            os.remove(cache_file)
        os.rename(temp_file, cache_file)

    logging.debug(f" -> {cache_file}")
    return cache_file


def parse_git_origin() -> GitLabOrigin:
    """Parse the origin remote's URL, so it can easily be used in API calls.
    When adjusting the output of this function, also adjust
    mrtest/origin.py.

    :returns: a GitLabOrigin object like the following:
              api: "https://gitlab.postmarketos.org/api/v4"
              api_project_id: "postmarketOS%2Fmrhlpr"
              full: "git@gitlab.postmarketos.org:postmarketOS/mrhlpr.git"
              project: "postmarketOS"
              project_id: "postmarketOS/mrhlpr"
              host: "gitlab.postmarketos.org"
              username: None
    """
    # Try to get the URL
    url = git.get_remote_url()
    if not url:
        print("Not inside a git repository, or no 'origin' remote configured.")
        exit(1)

    # Find the host
    domains = [
        "gitlab.alpinelinux.org",
        "gitlab.postmarketos.org",
        "gitlab.freedesktop.org",
        "gitlab.gnome.org",
        "invent.kde.org",
    ]
    for domain in domains:
        prefixes = [
            r"^git@(ssh\.)?" + domain.replace(".", "\\.") + ":",
            r"^https:\/\/(?:[^\s\@\/]*@)?" + domain.replace(".", "\\.") + "\\/",
        ]
        host = None
        rest = None
        for prefix in prefixes:
            if re.search(prefix, url):
                host = domain
                rest = re.sub(prefix, "", url)
                break
        if host:
            break
    if not host or not rest:
        print("Failed to extract gitlab server from: " + url)
        exit(1)

    # project_id: remove ".git" suffix
    project_id = rest
    if project_id.endswith(".git"):
        project_id = project_id[: -1 * len(".git")]

    # API URL parts
    api = "https://" + host + "/api/v4"
    api_project_id = urllib.parse.quote_plus(project_id)

    # Find username
    username_match = re.search(r"^https:\/\/([^\s\@\/]*)@gitlab\.com\/", url)
    username = username_match.group(1) if username_match else None
    project = project_id.split("/", 1)[0]

    # Return everything
    return GitLabOrigin(api, api_project_id, url, project, project_id, host, username)
