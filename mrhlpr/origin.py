# Copyright 2022 Oliver Smith
# SPDX-License-Identifier: GPL-3.0-or-later
"""Origin information about gitlab instances relevant to mrtest, in the format
needed for mrhlpr.gitlab.parse_git_origin()."""

import json
import logging
import os
import shutil
import urllib
import hashlib
from typing import Any, Optional


class GitLabOrigin:
    def __init__(
        self,
        api: str,
        api_project_id: str,
        full: str,
        project: str,
        project_id: str,
        host: str,
        username: Optional[str],
    ):
        self.api: str = api
        self.api_project_id: str = api_project_id
        self.full: str = full
        self.project: str = project
        self.project_id: str = project_id
        self.host: str = host
        self.username: Optional[str] = username

    def download_json(self, pathname: str, no_cache: bool = False) -> Any:
        """Download and parse JSON from an API, with a cache.

        :param pathname: gitlab URL pathname (without the usual prefix)
        :param no_cache: download again, even if already cached
        :returns: parsed JSON"""
        url = self.api + pathname

        home_path = os.getenv("HOME")
        if home_path is None:
            msg = "$HOME was not set"
            raise RuntimeError(msg)
        # Prepare cache
        cache_dir = home_path + "/.cache/mrhlpr/http"
        cache_key = hashlib.sha256(url.encode("utf-8")).hexdigest()
        cache_file = cache_dir + "/" + cache_key
        os.makedirs(cache_dir, exist_ok=True)

        # Check the cache
        if os.path.exists(cache_file) and not no_cache:
            logging.debug("Download " + url + " (cached)")
        else:
            print("Download " + url)
            # Save to temp file
            temp_file = cache_file + ".tmp"
            with urllib.request.urlopen(url) as response:
                with open(temp_file, "wb") as handle:
                    shutil.copyfileobj(response, handle)

            # Pretty print JSON (easier debugging)
            with open(temp_file, "r") as handle:
                parsed = json.load(handle)
            with open(temp_file, "w") as handle:
                handle.write(json.dumps(parsed, indent=4))

            # Replace cache file
            if os.path.exists(cache_file):
                os.remove(cache_file)
            os.rename(temp_file, cache_file)

        # Parse JSON from the cache file
        logging.debug(" -> " + cache_file)
        with open(cache_file, "r") as handle:
            return json.load(handle)


pmaports = GitLabOrigin(
    api="https://gitlab.postmarketos.org/api/v4",
    api_project_id="postmarketOS%2Fpmaports",
    full="git@gitlab.postmarketos.org:postmarketOS/pmaports.git",
    project="postmarketOS",
    project_id="postmarketOS/pmaports",
    host="gitlab.postmarketos.org",
    username=None,
)

aports = GitLabOrigin(
    api="https://gitlab.alpinelinux.org/api/v4",
    api_project_id="alpine%2Faports",
    full="git@gitlab.alpinelinux.org:alpine/aports.git",
    project="alpine",
    project_id="alpine/aports",
    host="gitlab.alpinelinux.org",
    username=None,
)
