# Copyright 2022 Oliver Smith
# SPDX-License-Identifier: GPL-3.0-or-later
"""Parse output from gitlab's pipeline api."""

import logging
import platform

from mrhlpr.origin import GitLabOrigin


class PipelineMetadata:
    def __init__(self, mr_id: int, no_cache: bool, origin: GitLabOrigin):
        # Get the latest pipeline (without cache so we don't miss newer ones)
        api = origin.download_json(
            f"/projects/{origin.api_project_id}/merge_requests/{mr_id}",
            no_cache=True,
        )
        if api.get("pipeline") is None:
            logging.error("ERROR: no pipeline found in merge request")
            exit(1)

        pipeline_id = api["pipeline"]["id"]
        pipeline_project_id = api["pipeline"]["project_id"]
        pipeline_status = api["pipeline"]["status"]

        if pipeline_status != "success":
            logging.warning(
                f"WARNING: pipeline id={pipeline_id} has unexpected"
                f" status '{pipeline_status}' instead of 'success'"
            )

        if origin.project == "alpine":
            trigger_jobs = origin.download_json(
                f"/projects/{pipeline_project_id}/pipelines/{pipeline_id}/bridges",
                True,
            )
            if trigger_jobs:
                if len(trigger_jobs) != 1:
                    raise RuntimeError(
                        f"Unexpected number of jobs: got {trigger_jobs} instead of 1"
                    )
                pipeline_id = trigger_jobs[0]["downstream_pipeline"]["id"]
                pipeline_project_id = trigger_jobs[0]["downstream_pipeline"]["project_id"]

        # Query the jobs of the pipeline (without cache, the result may change e.g.
        # if this ran earlier while the pipeline was not successful and now it is)
        api_pipeline_jobs = origin.download_json(
            f"/projects/{pipeline_project_id}/pipelines/{pipeline_id}/jobs",
            True,
        )
        job = self._get_build_job(api_pipeline_jobs)
        if not job:
            logging.error(
                "ERROR: could not find build job with device's architecture in the pipeline."
            )
            exit(1)

        self._host_arch_job: dict = job
        self._project_id: int = pipeline_project_id

    def _get_arch_alpine_native(self) -> str:
        """:returns: string for the architecture used in Alpine, e.g. 'armv7'"""
        machine = platform.machine()

        mapping = {
            "arm64": "aarch64",
            "armv6l": "armhf",
            "armv7l": "armv7",
            "i686": "x86",
        }
        if machine in mapping:
            return mapping[machine]
        return machine

    def _get_build_job(self, api_pipeline_jobs: dict) -> dict | None:
        """:param api_pipeline_jobs: dict from gitlab's pipelines/:id/jobs api
        :returns: job dict of the build job with the native arch or None"""

        build_native = f"build-{self._get_arch_alpine_native()}"

        for job in api_pipeline_jobs:
            if job["name"] in [build_native, f"{build_native}-emulated"]:
                return job

        return None

    @property
    def host_arch_job(self) -> dict:
        return self._host_arch_job

    @property
    def project_id(self) -> int:
        return self._project_id
