# Copyright 2022 Oliver Smith
# SPDX-License-Identifier: GPL-3.0-or-later
"""Pretty outputs and such."""

import argparse
from enum import Enum, unique
import logging
import sys
from typing import Any

try:
    import argcomplete
except ImportError:
    pass

from . import ci_labels  # type: ignore[attr-defined]
from . import git
from . import gitlab
from . import mr
from . import version


@unique
class ColorCode(str, Enum):
    OKGREEN = "\033[1;32m"
    ORANGE = "\033[93m"
    RED = "\033[91m"
    ENDC = "\033[0m"


@unique
class ActionType(str, Enum):
    OK = "[OK ]"
    NOK = "[NOK]"
    UNKNOWN = "[???]"

    @property
    def color(self) -> ColorCode:
        match self:
            case self.NOK:
                return ColorCode.RED
            case self.OK:
                return ColorCode.OKGREEN
            case self.UNKNOWN:
                return ColorCode.ORANGE
            case _:
                raise ValueError(f"No defined color for action {self}")

    def __str__(self) -> str:
        return self.value


def print_action(msg_type: ActionType, message: str) -> None:
    print(f"{msg_type.color.value}{msg_type} {message}{ColorCode.ENDC.value}")


def print_error(message: str) -> None:
    print(f"{ColorCode.RED.value}{message}{ColorCode.ENDC.value}")


def print_status(mr_id: int, no_cache: bool = False) -> None:
    """Print the merge request status. Most info is only visible, when the
    branch is checked out locally. Always display a checklist of things to
    do next, in order to get the MR shipped.

    :param mr_id: merge request ID
    :param no_cache: do not cache the API result for the merge request data
    """
    status = mr.get_status(mr_id, no_cache)
    is_checked_out = mr.checked_out() == mr_id
    is_rebased = None
    clean_worktree = None
    commits = []
    commits_have_id = None
    commits_follow_format = None
    subj_err: list[Any] = []
    commits_are_signed = None
    target_branch = status.target_branch

    # Generate URL
    origin = gitlab.parse_git_origin()
    url = "https://{}/{}/merge_requests/{}".format(origin.host, origin.project_id, mr_id)

    # Header
    print(url)
    print()
    print('"' + status.title + '"' + " (MR " + str(mr_id) + ")")
    if is_checked_out:
        is_rebased = git.is_rebased(target_branch)
        clean_worktree = git.clean_worktree(print_warning=True)
        commits = git.commits_on_top_of(target_branch)
        commits_have_id = mr.commits_have_mr_id(commits, mr_id)
        commits_follow_format, subj_err = mr.commits_follow_format(commits)
        commits_are_signed = mr.commits_are_signed(commits)
        print(
            "{} commit{} from {}/{}".format(
                len(commits),
                "s" if len(commits) > 1 else "",
                status.source_namespace,
                status.source_branch,
            )
        )
    else:
        print("not checked out, from " + status.source)
    print()

    if status.state == "closed":
        print_error("ERROR: MR has been closed.")
        exit(1)
    elif status.state == "merged":
        print_error("ERROR: MR has been merged.")
        exit(1)

    # Changes allowed by maintainers
    if status.allow_push:
        action = ActionType.OK
    else:
        action = ActionType.NOK
    print_action(action, "Changes allowed")

    # Clean worktree
    if clean_worktree is None:
        action = ActionType.UNKNOWN
    elif clean_worktree:
        action = ActionType.OK
    else:
        action = ActionType.NOK
    print_action(action, "Clean worktree")

    # Rebase on target branch
    if is_rebased is None:
        action = ActionType.UNKNOWN
    elif is_rebased:
        action = ActionType.OK
    else:
        action = ActionType.NOK
    print_action(action, f"Rebase on {target_branch}")

    # MR-ID in all commit messages
    if commits_have_id is None:
        action = ActionType.UNKNOWN
    elif commits_have_id:
        action = ActionType.OK
    else:
        action = ActionType.NOK
    print_action(action, "MR-ID in commit msgs")

    # All commits follow formatting
    if commits_follow_format is None:
        action = ActionType.UNKNOWN
    elif commits_follow_format:
        action = ActionType.OK
    else:
        action = ActionType.NOK
    print_action(action, "Commit subjects follow format")
    for line in subj_err:
        print("   " + line)

    # Commits are signed
    if commits_are_signed is None:
        action = ActionType.UNKNOWN
    elif commits_are_signed:
        action = ActionType.OK
    else:
        action = ActionType.NOK
    print_action(action, "Commits are signed")

    # Checklist
    print()
    print("Checklist:")
    if not status.allow_push:
        print_error(
            "* Ask MR author to tick 'Allow commits from members who can"
            " merge to the target branch.'"
        )
        print_error("* Check again ('mrhlpr -n status')")
        return

    if not is_checked_out:
        print_error("* Checkout this MR ('mrhlpr checkout " + str(mr_id) + "')")
        return

    if not clean_worktree:
        print_error("* Commit or stash changes in your worktree")
        print_error("* Check again ('mrhlpr status')")
        return

    if len(commits) > 1:
        print_error(
            f"* {len(commits)} commits: consider squashing ('git rebase -i origin/{target_branch}')"
        )

    if not is_rebased:
        print_error(f"* Rebase on {target_branch} ('git rebase origin/{target_branch}')")
        print_error("* Check again ('mrhlpr status')")
        return

    if not commits_have_id or not commits_are_signed:
        print_error("* Add the MR-ID to all commits and sign them ('mrhlpr fixmsg')")
        return

    if commits_follow_format is False:
        print_error("* Fix commit subjects that don't follow the correct formatting")
        return

    if commits_follow_format is None:
        print_error("* Manually check if the commit subjects are correct")

    print("* Pretty 'git log -" + str(len(commits)) + " --pretty'?" + " (consider copying MR desc)")
    print("* Finish the MR: push, approve, merge, comment ('mrhlpr finish')")


def get_parser() -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-n", "--no-cache", action="store_true", help="do not use local cache for MR information"
    )
    parser.add_argument(
        "-v",
        "--verbose",
        action="store_true",
        help="display debug log: all git commands and locations of http cache files",
    )
    parser.add_argument(
        "-V", "--version", action="version", version=version.get_full_version_information()
    )
    sub = parser.add_subparsers(title="action", dest="action")
    sub.required = True

    # Status
    status = sub.add_parser("status", help="show the MR status")
    status.add_argument("mr_id", type=int, nargs="?", help="merge request ID")

    # Checkout
    checkout = sub.add_parser("checkout", help="add and switch to the MR's branch")
    checkout.add_argument(
        "-n",
        "--no-fetch",
        action="store_false",
        dest="fetch",
        help="do not fetch the remote and origin repositories",
    )
    checkout.add_argument(
        "-o",
        "--overwrite-remote",
        action="store_true",
        help="overwrite the remote URLs if they differ",
    )
    checkout.add_argument("mr_id", type=int, help="merge request ID")

    # Fixmsg
    fixmsg = sub.add_parser("fixmsg", help="add the MR-ID to all commits and sign them")
    fixmsg.add_argument(
        "-b",
        "--skip-build",
        action="store_true",
        help=f"add message to last commit: {ci_labels['skip_build']}",
    )
    fixmsg.add_argument(
        "-c",
        "--ignore-count",
        action="store_true",
        help=f"add message to last commit: {ci_labels['ignore_count']}",
    )
    fixmsg.add_argument(
        "-v",
        "--skip-vercheck",
        action="store_true",
        help=f"add message to last commit: {ci_labels['skip_vercheck']}",
    )

    # push
    sub.add_parser("push", help="Push the MR to gitlab")

    # finish
    finish = sub.add_parser("finish", help="help finishing the MR (push, approve, merge...)")
    finish.add_argument(
        "-c",
        "--comment",
        type=str,
        nargs="?",
        help="comment to post before merging to thank author",
    )
    finish.add_argument(
        "-s",
        "--skip-approve",
        action="store_true",
        help="skips approving the MR before merging",
    )

    if "argcomplete" in sys.modules:
        argcomplete.autocomplete(parser, always_complete_options="long")
    return parser


def main() -> None:
    args = get_parser().parse_args()
    if args.verbose:
        logging.getLogger().setLevel(logging.DEBUG)
    if args.action == "status":
        mr_id = args.mr_id if args.mr_id else mr.checked_out()
        print_status(mr_id, args.no_cache)
    elif args.action == "checkout":
        mr.checkout(args.mr_id, args.no_cache, args.fetch, args.overwrite_remote)
        print_status(args.mr_id)
    elif args.action == "fixmsg":
        mr_id = mr.checked_out()
        mr.fixmsg(mr_id, args.skip_build, args.ignore_count, args.skip_vercheck)
        print_status(mr_id)
    elif args.action == "push":
        mr_id = mr.checked_out()
        mr.push(mr_id, args.no_cache)
    elif args.action == "finish":
        mr_id = mr.checked_out()
        # TODO: Check that everything passed
        comment = args.comment
        if not comment:
            comment = input("Provide a thank you comment (can be empty — discouraged): ")
        mr.finish(mr_id, comment, args.no_cache, args.skip_approve)
        print_status(mr_id)
