from typing import Final

VERSION: Final[str] = "1.7.0"


def get_full_version_information() -> str:
    """Provides a full version string, including an educated guess about whether the program was
    compiled by mypyc."""
    return f"{VERSION} (compiled: {'yes' if is_mypyc_compiled() else 'no'})"


def is_mypyc_compiled() -> bool:
    """Attempts to determine whether the program was compiled by mypyc."""
    return not __file__.endswith(".py")
