#!/usr/bin/env python3
# Copyright 2022 Oliver Smith
# SPDX-License-Identifier: GPL-3.0-or-later
"""Internally used to add the MR-ID to a given commit message."""

import os
import sys

mrhlpr_msg_filter_mr_id = os.getenv("MRHLPR_MSG_FILTER_MR_ID")

if not mrhlpr_msg_filter_mr_id:
    print("This script is meant to be called internally by mrhlpr.py.")
    print("It accepts a commit message on stdin, and writes it back with")
    print("the merge request ID appended to the subject line.")
    print("ERROR: MRHLPR_MSG_FILTER_MR_ID is not set")
    exit(1)


line_count = 0
empty_lines = 0
trailer = f"Part-of: {mrhlpr_msg_filter_mr_id}"
emit_trailer = True
have_trailers = False
for line in sys.stdin:
    line = line.rstrip()
    line_count += 1

    # Don't emit the trailer if the commit already has it
    if line == trailer:
        emit_trailer = False
    else:
        # Count empty lines since the last non-empty line
        if line == "":
            empty_lines += 1
        else:
            empty_lines = 0

        if any(
            line.lower().startswith(prefix)
            for prefix in [
                "signed-off-by: ",
                "co-developed-by: ",
                "suggested-by: ",
                "reported-by: ",
                "change-id: ",
                "co-authored-by: ",
                "part-of: ",
            ]
        ):
            have_trailers = True

    print(line)

if emit_trailer:
    # Print a blank line before the trailer section unless we already have trailers
    if empty_lines == 0 and not have_trailers:
        print()
    print(trailer)
