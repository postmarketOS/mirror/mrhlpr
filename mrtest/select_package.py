# Copyright 2022 Oliver Smith
# SPDX-License-Identifier: GPL-3.0-or-later
"""Interactive package selection dialog"""

import os
import zipfile

import mrtest.apk_installed


def get_apks_in_zip(zip_path: str) -> list[str]:
    """
    :param zip_path: downloaded artifacts zip containing the apks
    :returns: list of paths inside the zip file like:
              ["packages/edge/aarch64/postmarketos-ui-phosh-18-r0.apk"m ...]
    """
    ret = []
    with zipfile.ZipFile(zip_path) as zip:
        for path in zip.namelist():
            if path.endswith(".apk"):
                ret += [path]
    return ret


def show_selection_short(apks: list[str], ret: list[str]) -> None:
    """
    Print a short summary of how many packages have been selected.
    :param apks: list of all apks inside the zip file
    :param ret: list of selected apks inside the zip file
    """
    print(f"{len(ret)} of {len(apks)} selected.")


def show_selection(apks: list[str], ret: list[str]) -> None:
    """
    Print the list of available packages, with a checkbox for each package.
    :param apks: list of all apks inside the zip file
    :param ret: list of selected apks inside the zip file
    """
    i = 1
    for apk in apks:
        sel = "X" if apk in ret else " "

        installed = mrtest.apk_installed.get_installed(apk)
        installed_str = ""
        if installed:
            installed_str = f" [installed: {installed}]"
        print(f"  {i}) [{sel}] {os.path.basename(apk)}{installed_str}")
        i += 1

    print("")


def toggle_installed(apks: list[str], ret: list[str]) -> list[str]:
    """
    Toggle all already instaleld packages.
    :param apks: list of all apks inside the zip file
    :param ret: list of selected apks inside the zip file
    :returns: updated version of ret
    """
    already_selected = True
    apks_installed = []

    for apk in apks:
        installed = mrtest.apk_installed.get_installed(apk)
        if installed:
            apks_installed += [apk]
        if apk not in ret:
            already_selected = False

    # Unselect all packages that are already installed
    if already_selected:
        ret = [x for x in ret if x not in apks_installed]
        return ret

    # Select all packages that are already installed
    for apk in apks_installed:
        if apk not in ret:
            ret += [apk]
    return ret


def show_commands() -> None:
    print("Commands:")
    print("  1-99: toggle this package")
    print("  a:    toggle all packages")
    print("  u:    toggle upgrade (or downgrade) of installed packages")
    print("  l:    list selection")
    print("  y:    confirm selection")
    print("  q:    quit")
    print("")
    print("Toggling multiple packages in one command is possible (e.g. 2 3 4 10-15 20)")


def toggle_package(selection: int | str, apks: list[str], ret: list[str]) -> list[str]:
    """Toggle the package based on the user action.
    :param selection: user input, single integer
    :param apks: list of packages available for install
    :param ret: list containing packages that are marked for install"""
    pkg = int(selection)
    if pkg >= 1 and pkg <= len(apks):
        apk = apks[pkg - 1]
        if apk in ret:
            ret = [x for x in ret if x != apk]
        else:
            ret += [apk]
    else:
        print(f"Number {pkg} is out of range (1-{len(apks)})! Ignoring...")
    return ret


def toggle_packages(action: str, apks: list[str], ret: list[str]) -> list[str]:
    """Toggle multiple packages based on the user action.
    :param action: user input, multiple integers or range
    :param apks: list of packages available for install
    :param ret: list containing packages that are marked for install"""
    selection = action.split()
    for i in selection:
        multiple = i.split("-")
        size = len(multiple)
        if size == 1 and i.isnumeric():
            ret = toggle_package(i, apks, ret)
        elif size == 2 and multiple[0].isnumeric() and multiple[1].isnumeric():
            start = int(multiple[0])
            stop = int(multiple[1]) + 1
            for j in range(start, stop):
                ret = toggle_package(j, apks, ret)
        else:
            print(f"Don't know ({i})! Ignoring...")
    return ret


def ask(zip_path: str) -> list[str]:
    """Ask the user which packages shall be installed or upgraded.

    :param zip_path: downloaded artifacts zip containing the apks
    :returns: paths inside the zip file of the packages that the user wants
              to install, e.g. ["packages/edge/aarch64/postmarketos-ui-phosh-18-r0.apk"]"""
    ret: list[str] = []
    apks = get_apks_in_zip(zip_path)

    # Fill up cache and log message of getting these here
    mrtest.apk_installed.get_installed_all()

    print("Which packages to install?")
    print("")
    show_selection(apks, ret)
    show_selection_short(apks, ret)
    print()
    show_commands()
    print()

    while True:
        action = input("What now?> ")
        if action == "a":
            if len(ret) == len(apks):
                ret = []
            else:
                ret = apks.copy()
        elif action == "h":
            show_commands()
        elif action == "l":
            show_selection(apks, ret)
        elif action == "y":
            print("Selection confirmed")
            return ret
        elif action == "q":
            print("Quitting")
            exit(1)
        elif action == "u":
            ret = toggle_installed(apks, ret)
        elif " " in action or "-" in action:
            ret = toggle_packages(action, apks, ret)
        elif action.isnumeric():
            ret = toggle_package(action, apks, ret)
        elif len(action) > 0:
            print(f"Don't know ({action}), try 'h' for help")
        show_selection_short(apks, ret)

    raise AssertionError("This should never be reached")
