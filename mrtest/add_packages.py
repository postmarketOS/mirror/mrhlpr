# Copyright 2022 Oliver Smith
# SPDX-License-Identifier: GPL-3.0-or-later
"""High level action of adding packages from MR's artifacts"""

import logging
import os
import shutil
import zipfile
import subprocess
from typing import Literal
from urllib.error import HTTPError

import mrhlpr.mr
from mrhlpr.origin import GitLabOrigin
import mrtest.select_package


def extract_apks(zip_path: str, selection: list[str]) -> list[str]:
    """
    :param zip_path: to the downloaded artifacts zip
    :param selection: list of apks inside the zip archive
    :returns: list of full paths to extracted apk files
    """
    print("Extracting packages from artifacts archive...")
    extract_dir = f"{os.getenv('HOME')}/.cache/mrhlpr/apks"
    temp_dir = f"{extract_dir}/.temp"
    if os.path.exists(extract_dir):
        shutil.rmtree(extract_dir)
    os.makedirs(extract_dir, exist_ok=True)

    ret = []
    with zipfile.ZipFile(zip_path) as zip:
        for apk in selection:
            target = f"{extract_dir}/{os.path.basename(apk)}"
            logging.debug(f"Extract {target}")

            # 'zip.extract' appends the full path inside the archive to the
            # output path. The subdirectories are not useful here, so extract
            # to a temp_dir first, then move the apk from subdirs of temp_dir
            # to the target path, then remove the temp_dir.
            zip.extract(apk, temp_dir)
            os.rename(f"{temp_dir}/{apk}", target)
            shutil.rmtree(temp_dir)

            ret += [target]

    return ret


def run_apk_add(origin: GitLabOrigin, mr_id: int, apk_paths: list[str]) -> None:
    """
    :param origin: gitlab origin information, see gitlab.parse_git_origin()
    :param mr_id: merge request ID
    :param apk_paths: list of apk file paths to be installed
    """
    cmd = [
        "apk",
        "add",
        "-u",
        "--virtual",
        mrtest.get_virtual_group(origin, mr_id),  # type: ignore[attr-defined]
        "--allow-untrusted",
    ] + apk_paths
    if not mrtest.is_root_user():  # type: ignore[attr-defined]
        cmd = [mrtest.get_sudo()] + cmd  # type: ignore[attr-defined]

    print("Installing packages...")
    logging.debug(f"+ {cmd}")
    subprocess.run(cmd, check=True)


def confirm_mr_id(origin: GitLabOrigin, mr_id: int, action: Literal["add", "upgrade"]) -> None:
    """
    :param origin: gitlab origin information, see gitlab.parse_git_origin()
    :param mr_id: merge request ID
    """
    link = f"https://{origin.host}/{origin.project_id}/-/merge_requests/{mr_id}"

    status = mrhlpr.mr.get_status(mr_id, origin=origin)
    action_description = "select and then install" if action == "add" else "upgrade"

    print("Welcome to mrtest, this tool allows downloading and installing")
    print("Alpine packages from merge requests.")
    print()
    print("WARNING: do not use this tool unless you understand what it does,")
    print("otherwise you can get tricked into installing malicious code!")
    print("Malicious code may make your device permanently unusable, steal")
    print("your passwords, and worse.")
    print()
    print(f"You are about to {action_description} packages from:")
    print(link)
    print("\t“\033[1m" + status.title + "\033[m”")
    print("\t(\033[3m" + status.source + ":" + status.source_branch + "\033[m)")
    print()
    print("* Did you read and understand the diff?")
    print("* If not, do you at least trust the person who submitted it?")
    print()
    print("If you don't understand the diff and don't trust the submitter,")
    print("answer with 'stop'. Otherwise, if you want to proceed, type in the")
    print("MR ID.")
    print()
    mr_id_input = input("Your answer: ")
    if mr_id_input != str(mr_id):
        print("Aborted.")
        exit(1)
    print("---")


def add_packages(origin: GitLabOrigin, mr_id: int, no_cache: bool) -> None:
    """
    :param origin: gitlab origin information, see gitlab.parse_git_origin()
    :param mr_id: merge request ID
    :param no_cache: instead of using a cache for api calls / downloads where
                     it makes sense, always download a fresh copy
    """
    confirm_mr_id(origin, mr_id, "add")

    try:
        zip_path = mrhlpr.mr.get_artifacts_zip(mr_id, no_cache, origin)
    except (HTTPError, AttributeError):
        print(f"\r\nRemote server does not have any packages to download for MR {mr_id}!")
        print("\r\nThis could be caused by:")
        print(" * use of [CI:skip-build] in the last commit")
        print(" * CI pipeline failure")
        print(" * MR is in draft status and pipelines have not run yet.")
        print(f"Please trigger again the MR {mr_id} to generate the packages.\r\n")
        exit(0)

    selection = mrtest.select_package.ask(zip_path)
    if selection == []:
        print("No packages selected.")
        exit(0)

    apk_paths = extract_apks(zip_path, selection)
    try:
        run_apk_add(origin, mr_id, apk_paths)
    except subprocess.CalledProcessError:
        print("Failed to install packages!")
        exit(1)

    print("All done! Use 'mrtest zap' to uninstall added packages.")
