# Copyright 2022 Oliver Smith
# SPDX-License-Identifier: GPL-3.0-or-later
"""Parse command-line arguments."""

import argparse
import logging
import sys

import mrhlpr.version
import mrhlpr.origin
import mrtest.add_packages
import mrtest.upgrade_packages
import mrtest.zap_packages

try:
    import argcomplete
except ImportError:
    pass


def arguments_add(sub: argparse._SubParsersAction) -> None:
    """:param sub: argparser's subparser"""
    parser = sub.add_parser("add", help="install packages from an MR")
    parser.add_argument(
        "-a", "--alpine", action="store_true", help="use alpine's aports instead of pmOS' pmaports"
    )
    parser.add_argument("mr_id", type=int, help="merge request ID")


def arguments_upgrade(sub: argparse._SubParsersAction) -> None:
    """:param sub: argparser's subparser"""
    parser = sub.add_parser("upgrade", help="upgrade to packages from an MR")
    parser.add_argument(
        "-a", "--alpine", action="store_true", help="use alpine's aports instead of pmOS' pmaports"
    )
    parser.add_argument(
        "-l",
        "--available",
        action="store_true",
        help="install remote versions of packages even if they aren't a higher version",
    )
    parser.add_argument("mr_id", type=int, help="merge request ID")


def arguments_zap(sub: argparse._SubParsersAction) -> None:
    """:param sub: argparser's subparser"""
    sub.add_parser("zap", help="uninstall previously added packages")


def get_parser() -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-n", "--no-cache", action="store_true", help="do not use local cache for MR information"
    )
    parser.add_argument(
        "-v",
        "--verbose",
        action="store_true",
        help="display debug log: all commands and locations of http cache files",
    )
    parser.add_argument(
        "-V", "--version", action="version", version=mrhlpr.version.get_full_version_information()
    )
    sub = parser.add_subparsers(title="action", dest="action")
    sub.required = True

    arguments_add(sub)
    arguments_upgrade(sub)
    arguments_zap(sub)

    if "argcomplete" in sys.modules:
        argcomplete.autocomplete(parser, always_complete_options="long")
    return parser


def main() -> None:
    args = get_parser().parse_args()
    if args.verbose:
        logging.getLogger().setLevel(logging.DEBUG)
    if args.action == "zap":
        mrtest.zap_packages.zap_packages()
        return
    origin = mrhlpr.origin.aports if args.alpine else mrhlpr.origin.pmaports
    if args.action == "add":
        mrtest.add_packages.add_packages(origin, args.mr_id, args.no_cache)
    elif args.action == "upgrade":
        mrtest.upgrade_packages.upgrade_from_mr(origin, args.mr_id, args.alpine, args.available)
