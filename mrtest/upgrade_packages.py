# Copyright 2024 Stefan Hansson
# SPDX-License-Identifier: GPL-3.0-or-later

import logging
import subprocess

import mrtest
from mrhlpr.mr import get_artifacts_repo_urls
from mrhlpr.origin import GitLabOrigin
from mrtest.add_packages import confirm_mr_id


def upgrade_from_mr(origin: GitLabOrigin, mr_id: int, alpine_mr: bool, available: bool) -> None:
    confirm_mr_id(origin, mr_id, "upgrade")
    repo_urls = get_artifacts_repo_urls(mr_id, True, origin, alpine_mr)

    repo_args = []

    for repo_url in repo_urls:
        repo_args += ["-X", repo_url]

    cmd = [
        "apk",
        "upgrade",
        *repo_args,
        "--allow-untrusted",
        "--force-missing-repositories",
        "--interactive",
    ]

    if available:
        cmd.append("--available")

    if not mrtest.is_root_user():  # type: ignore[attr-defined]
        cmd = [mrtest.get_sudo()] + cmd  # type: ignore[attr-defined]

    print("Upgrading packages...")
    logging.debug(f"+ {cmd}")
    subprocess.run(cmd, check=True)
