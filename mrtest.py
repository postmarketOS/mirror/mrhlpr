#!/usr/bin/env python3
# Copyright 2022 Oliver Smith
# SPDX-License-Identifier: GPL-3.0-or-later
# PYTHON_ARGCOMPLETE_OK

import mrtest.frontend

if __name__ == "__main__":
    mrtest.frontend.main()
