Welcome to the mrhlpr & mrtest documentation!
==========================================================

mrhlpr and mrtest are two small applications supplementing the work with merge requests on the GitLab repositories.

For the latest releases please check the `git repository`_.

In case of any problems that is also the place you will find the `issue tracker`_.

For further information, please check out the `postmarketOS-wiki`_.


.. toctree::
   :maxdepth: 3
   :caption: Contents:

   installation
   usage
   mrhlpr/modules
   mrtest/modules



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`


*Note:* This documentation is currently a work-in-progress, your feedback and contributions are very welcome!

.. _postmarketOS-wiki: https://wiki.postmarketos.org/wiki/Main_Page
.. _issue tracker: https://gitlab.postmarketos.org/postmarketOS/mrhlpr/-/issues
.. _git repository: https://gitlab.postmarketos.org/postmarketOS/mrhlpr/
