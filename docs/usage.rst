#####
Usage
#####

**mrhlpr** and **mrtest** offer some options and actions and are normally ran from a shell.

mrhlpr
######

mrhlpr is the tool for handling MR (merge requests) during development.
It can be used to checkout branches from MR on supported GitLab instances.
After checkout, it offers options to interact with MR like listed below.


.. autoprogram:: mrhlpr.frontend:get_parser()
   :prog: mrhlpr
   :groups:


mrtest
######

mrtest is the tool for testing the content of an MR on a postmarketOS or Alpine Linux system before the MR has been merged. 
It supports interacting with both Alpine's aports and postmarketOS' pmaports.
This is done by using the artifacts from the CI runs as source to get packages which are installed locally.

**Installing packages**

Navigate to a merge request that you want to test.
After reading through the changes made in the merge request, run:

.. code-block::

 $ mrtest add MR_ID.

replacing MR_ID with the number of the MR
It will automatically figure out the last pipeline, get the job with the suitable arch of your device, and show which packages are available for installation. Then you will be able to select the packages in an interactive prompt.

To install packages from a merge request in Alpine, use `mrtest add -a` (or --alpine).

**Example**

Trying `!62 <https://gitlab.postmarketos.org/postmarketOS/mobile-config-firefox/-/merge_requests/62>`_

.. code-block::

 $ mrtest add 62
 Download https://gitlab.com/api/v4/projects/postmarketOS%2Fpmaports/merge_requests/62
 Download https://gitlab.com/api/v4/projects/8986050
 Welcome to mrtest, this tool allows downloading and installing
 Alpine packages from merge requests.

 WARNING: do not use this tool unless you understand what it does,
 otherwise you can get tricked into installing malicious code!
 Malicious code may make your device permanently unusable, steal
 your passwords, and worse.

 You are about to select and then install packages from:
 https://gitlab.com/postmarketOS/pmaports/-/merge_requests/62
	“wingtech-wt88047: initial support (xiaomi redmi 2)”
	(sylvieon/pmaports:device/wingtech-wt88047)

 * Did you read and understand the diff?
 * If not, do you at least trust the person who submitted it?

 If you don't understand the diff and don't trust the submitter,
 answer with 'stop'. Otherwise, if you want to proceed, type in the
 MR ID.

 Your answer: 

Now you enter `62` and follow to promts until the full mr is installed with returning:

.. code-block::

 All done! Use 'mrtest zap' to uninstall added packages.



**Uninstalling packages**

Use `mrtest zap` to check for previously installed virtual packages, and to get a prompt to uninstall them. 
Uninstalling virtual packages will remove all packages that were added with a mrtest add command.
Note that this does not include upgraded packages, to remove upgrade packages you will need to run `apk upgrade -a` as the output at the end of `mrtest zap` explains. 

.. code-block::

  $ mrtest zap          
 Getting installed packages...
 Virtual packages from previous 'mrtest add':
 * .mrtest-61-postmarketOS
 Remove virtual packages and packages added with mrtest? [y/N] y

Proceed with entering `y` and your system is back to the original state.

.. note::

 Packages installed through `mrtest add` won't be automatically upgraded. You'll need to `mrtest zap` them before you'd be able to install newer versions from the official binary repo later using `apk upgrade`!


.. autoprogram:: mrtest.frontend:get_parser()
   :prog: mrtest
   :groups:



Requirements
############

mrhlpr requires the following:

  * Linux distribution on the host system
  * Python 3.9+
  * python-gitlab>=4.0.0 (optional, but strongly recommended)

mrtest requires the following:

  * system with postmarketOS or Alpine Linux
  * Python 3.9+
