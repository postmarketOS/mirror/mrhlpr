############################
Installation on PostmarketOS
############################

From Alpine's package manager

.. code-block::

 $ apk add mrtest


Or from the git repository

.. code-block::

 $ git clone https://gitlab.postmarketos.org/postmarketOS/mrhlpr.git


mrtest source code lives inside `mrhlpr.git <https://gitlab.postmarketos.org/postmarketOS/mrhlpr>`_ , it shares code for interacting with the gitlab API with the mrhlpr project.

