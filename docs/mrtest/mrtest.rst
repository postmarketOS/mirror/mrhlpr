mrtest package
==============

Submodules
----------

mrtest.add_packages module
--------------------------

.. automodule:: mrtest.add_packages
   :members:
   :undoc-members:
   :show-inheritance:

mrtest.apk_installed module
---------------------------

.. automodule:: mrtest.apk_installed
   :members:
   :undoc-members:
   :show-inheritance:

mrtest.frontend module
----------------------

.. automodule:: mrtest.frontend
   :members:
   :undoc-members:
   :show-inheritance:

mrtest.select_package module
----------------------------

.. automodule:: mrtest.select_package
   :members:
   :undoc-members:
   :show-inheritance:

mrtest.upgrade_packages module
------------------------------

.. automodule:: mrtest.upgrade_packages
   :members:
   :undoc-members:
   :show-inheritance:

mrtest.zap_packages module
--------------------------

.. automodule:: mrtest.zap_packages
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: mrtest
   :members:
   :undoc-members:
   :show-inheritance:
